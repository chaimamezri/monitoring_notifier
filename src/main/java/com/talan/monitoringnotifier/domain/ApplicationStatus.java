package com.talan.monitoringnotifier.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "ApplicationStatus")
public class ApplicationStatus {
@Id
	private String id;
	private String applicationName;
	private String applicationHealth;
	private String databaseStatus;
	private Date timestamp;
	private String notificationStatus;
	private NotificationType notificationType;

	public void setNotificationType(String typeNotification) {
		if (typeNotification.equals("EMAIL"))
			this.notificationType = NotificationType.EMAIL;

		else if (typeNotification.equals("SMS"))
			this.notificationType = NotificationType.SMS;
		else
			this.notificationType = null;
	}

}
