package com.talan.monitoringnotifier.domain;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document ( collection ="HardwareMetrics")
public class HardwareMetrics 
{
	
	private String id;
	private String applicationName;
	private long usedMemoryBytes;
	private float systemCPUsagePercent;
	private long totalDiskSpace;
	private long freeDiskSpace;
	private long thresholdDiskSpace;
	private Date  timestamp;
	private String notificationStatus;
	private NotificationType notificationType;
	
}
