package com.talan.monitoringnotifier.domain;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmailNotification {
	@Id 
	private String name;
	private String subject;
	private String content;
	private String contentType;
	private List < Object > attachments;
	private String emailFrom;
	private String emailCc;
	private List<String> emailTo;
	private boolean enable;
	
	 public EmailNotification() {
	        contentType = "text/plain";
	    }
}
