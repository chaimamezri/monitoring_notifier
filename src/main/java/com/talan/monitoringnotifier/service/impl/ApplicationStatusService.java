package com.talan.monitoringnotifier.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.talan.monitoringnotifier.domain.ApplicationStatus;
import com.talan.monitoringnotifier.domain.EmailNotification;
import com.talan.monitoringnotifier.domain.NotificationType;
import com.talan.monitoringnotifier.repository.ApplicationStatusRepository;
import com.talan.monitoringnotifier.repository.EmailNotificationRepository;



@EnableScheduling
@Service
public class ApplicationStatusService implements IApplicationStatusService{
	@Autowired
	private ApplicationStatusRepository applicationStatusRepository;
	private static final Logger L=LogManager.getLogger(ApplicationStatusService.class);
	@Autowired
	private EmailNotificationRepository emailNotificationRepository;
	@Autowired
	private JavaMailSender mailSender;

	public List<ApplicationStatus> getApplicationStatus() {
		return applicationStatusRepository.findAll();
	}

	@Scheduled(fixedRateString = "${fixedRate.applyRules.in.milliseconds}")
	public void processingEvent() {
		// System.out.println("************** processingEvent running every 5
		// secondes");
		List<ApplicationStatus> applicationStatusList = this.getApplicationStatus();
		Stream<ApplicationStatus> applicationStatusStream = applicationStatusList.stream();
		applicationStatusStream.filter(x -> x.getNotificationStatus().isEmpty())
				.map((Function<? super ApplicationStatus, ? extends ApplicationStatus>) x -> {
					ApplicationStatus s = applyRules(x);
					System.out.println("after processing x= " + x);
					return applicationStatusRepository.save(s);
				}).forEach(x -> {
					System.out.println("after processing x= " + x);
					// applicationStatusRepository.save(x);
					if (x.getNotificationType().equals(NotificationType.EMAIL))
						sendEmailNotification(x);
					if (x.getNotificationType().equals(NotificationType.SMS))
						sendSMSNotification(x);
				});
	}

	public void sendEmailNotification(ApplicationStatus applicationStatus) {
		System.out.println("************** Sending NotificationEmail ");
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		Optional<EmailNotification> emailNotif = emailNotificationRepository
				.findById(applicationStatus.getNotificationStatus());
		if (emailNotif.isPresent()) {
			try {

				MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

				mimeMessageHelper.setSubject(emailNotif.get().getSubject());
				mimeMessageHelper.setFrom(new InternetAddress(emailNotif.get().getEmailFrom(), "TalanMonitoring.com"));
				String[] emailsTo = emailNotif.get().getEmailTo().stream().toArray(String[]::new);
//				mimeMessageHelper.setTo(emailsTo);
				mimeMessageHelper.setTo("chaima.mezri@esprit.tn");
				mimeMessageHelper.setText(emailNotif.get().getContent());
				mailSender.send(mimeMessageHelper.getMimeMessage());

			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}

	public static void sendSMSNotification(ApplicationStatus ApplicationStatus) {
		System.out.println("************** Sending NotificationSMS");
	}

	static Map<String, KieBase> kieBaseCache = null;
	static {
		kieBaseCache = new HashMap<>();
	}

	public static ApplicationStatus applyRules(ApplicationStatus applicationStatus) {
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*- Entry applyRules ApplicationStatus= " + applicationStatus);
		KieSession kieSession = null;
		String content = null;
		try {
			content = new String(Files.readAllBytes(Paths.get("src/main/resources/com/talan/rules/Rules.drl")),
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// System.out.println("Read New Rules set from File");
		// load up the knowledge base

		KieServices ks = KieServices.Factory.get();
		String inMemoryDrlFileName = "src/main/resources/com/talan/rules/stateFulSessionRule.drl";
		KieFileSystem kfs = ks.newKieFileSystem();
		kfs.write(inMemoryDrlFileName,
				ks.getResources().newReaderResource(new StringReader(content)).setResourceType(ResourceType.DRL));
		KieBuilder kieBuilder = ks.newKieBuilder(kfs).buildAll();

		if (kieBuilder.getResults().hasMessages(Message.Level.ERROR)) {
			System.out.println(kieBuilder.getResults().toString());
		}

		KieContainer kContainer = ks.newKieContainer(kieBuilder.getKieModule().getReleaseId());
		KieBaseConfiguration kbconf = ks.newKieBaseConfiguration();
		KieBase kbase = kContainer.newKieBase(kbconf);
		kieSession = kbase.newKieSession();
		// System.out.println("Put rules KieBase into Custom Cache");
		kieBaseCache.put("validateRules", kbase);
		// System.out.println("Get existing rules KieBase from Custom Cache");
		kieSession = kieBaseCache.get("validateRules").newKieSession();
		kieSession.insert(applicationStatus);
		kieSession.fireAllRules();
		kieSession.dispose();// refresh
		System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*- application des regles= " + applicationStatus);
		return applicationStatus;

	}

	

	@Override
	public ApplicationStatus addApplicationStatus(ApplicationStatus A) {
		// TODO Auto-generated method stub
		return  applicationStatusRepository.save(A);
	}

	@Override
	public void deleteApplicationStatus(String id) {
		applicationStatusRepository.deleteById(id);
		
	}

	@Override
	public ApplicationStatus updateApplicationStatus(ApplicationStatus A) {
		 return applicationStatusRepository.save(A);
	}

	@Override
	public ApplicationStatus getApplicationStatus(String id) {
		return applicationStatusRepository.findById(id).orElse(null);
	}

	@Override
	public List<ApplicationStatus> getAllApplications() {
		List<ApplicationStatus> Applications= (List<ApplicationStatus>) applicationStatusRepository.findAll();
		L.info( Applications.toString());
		return  Applications;	}

	

	

	

}
