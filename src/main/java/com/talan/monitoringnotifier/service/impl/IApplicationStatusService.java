package com.talan.monitoringnotifier.service.impl;

import java.util.List;

import com.talan.monitoringnotifier.domain.ApplicationStatus;


public interface IApplicationStatusService {

List<ApplicationStatus> getAllApplications();

	ApplicationStatus addApplicationStatus(ApplicationStatus A);

	void deleteApplicationStatus(String id);

	ApplicationStatus updateApplicationStatus(ApplicationStatus A);

	ApplicationStatus getApplicationStatus(String id);
}
