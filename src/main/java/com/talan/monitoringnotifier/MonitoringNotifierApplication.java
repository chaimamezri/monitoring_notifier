package com.talan.monitoringnotifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoringNotifierApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringNotifierApplication.class, args);
	}

}
