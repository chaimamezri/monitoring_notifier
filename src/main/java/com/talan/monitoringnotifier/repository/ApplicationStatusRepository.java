package com.talan.monitoringnotifier.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.talan.monitoringnotifier.domain.ApplicationStatus;

@Repository
public interface ApplicationStatusRepository extends MongoRepository<ApplicationStatus, String> 
{

	
}
