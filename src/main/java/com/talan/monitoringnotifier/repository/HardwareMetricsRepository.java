package com.talan.monitoringnotifier.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.talan.monitoringnotifier.domain.HardwareMetrics;

public interface HardwareMetricsRepository extends MongoRepository<HardwareMetrics, String>
{

}
