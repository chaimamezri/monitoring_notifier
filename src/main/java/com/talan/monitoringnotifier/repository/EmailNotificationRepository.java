package com.talan.monitoringnotifier.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.talan.monitoringnotifier.domain.EmailNotification;

public interface EmailNotificationRepository extends MongoRepository<EmailNotification, String>{

}
