package com.talan.monitoringnotifier.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.monitoringnotifier.domain.EmailNotification;
import com.talan.monitoringnotifier.repository.EmailNotificationRepository;

@CrossOrigin("*")
@RestController
@RequestMapping({"/emailNotification"})
public class EmailNotificationController 
{
	@Autowired
	public EmailNotificationRepository emailNotificationRepository;
	
	@PostMapping (value="/register")
	public EmailNotification insertEmailNotification (@RequestBody EmailNotification emailNotification) {
	 return  emailNotificationRepository.save(emailNotification);
	}
	 
}