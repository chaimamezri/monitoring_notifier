package com.talan.monitoringnotifier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.talan.monitoringnotifier.domain.ApplicationStatus;
import com.talan.monitoringnotifier.repository.ApplicationStatusRepository;
import com.talan.monitoringnotifier.service.impl.ApplicationStatusService;








@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping({"/applicationStatus"})
public class ApplicationStatusController 
{
	@Autowired
	public ApplicationStatusRepository applicationStatusRepository;
	
	@Autowired
	public ApplicationStatusService applicationStatusService;
	
	 @Autowired 
	 ApplicationStatusService IApplicationStatusService;
	
	@PostMapping (value="/register")
	public ApplicationStatus insertApplicationStatus (@RequestBody ApplicationStatus applicationStatus) {
	 return  applicationStatusRepository.save(applicationStatus);
	}
	

	
	@GetMapping (value="/sendEmailNotification")
	public String sendEmailNotification(){
		List<ApplicationStatus> applicationStatusList = applicationStatusService.getApplicationStatus();
		for(ApplicationStatus appStatus : applicationStatusList)
			applicationStatusService.sendEmailNotification(appStatus);
		
		return "emailsent";
	}
	
	
	
	
	@GetMapping("/applications")
	@ResponseBody
	public List<ApplicationStatus> getApplicationStatus() {
		return IApplicationStatusService.getAllApplications();
	}
	
	
	@GetMapping("/get-applicationStatus/{applicationStatus-id}")  
	@ResponseBody
	public ApplicationStatus getapplicationStatus(@PathVariable("applicationStatus-id") String applicationStatusid) {
		return IApplicationStatusService.getApplicationStatus(applicationStatusid);
	}

	@PostMapping("/add-ApplicationStatus")
	@ResponseBody	
	public ApplicationStatus addApplicationStatus(@RequestBody ApplicationStatus A) {
	       return IApplicationStatusService.addApplicationStatus(A);

	}
	@DeleteMapping("/delete-ApplicationStatus/{ApplicationStatus-id}")
	@ResponseBody
	public void deleteApplicationStatus(@PathVariable("ApplicationStatus-id") String applicationStatusid) {
		IApplicationStatusService.deleteApplicationStatus(applicationStatusid);
	}

	
		@PutMapping("/update-post")
		@ResponseBody
		public ApplicationStatus modifyApplicationStatus(@RequestBody ApplicationStatus applicationStatus) {
			return IApplicationStatusService.updateApplicationStatus(applicationStatus);
		}

}
