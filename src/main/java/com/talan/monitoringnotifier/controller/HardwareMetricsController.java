package com.talan.monitoringnotifier.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talan.monitoringnotifier.domain.HardwareMetrics;
import com.talan.monitoringnotifier.repository.HardwareMetricsRepository;

@CrossOrigin("*")
@RestController
@RequestMapping({"/hardwareMetrics"})
public class HardwareMetricsController {
	@Autowired
	public HardwareMetricsRepository hardwareMetricsRepository;
	
	@PostMapping (value="/register")
	public HardwareMetrics insertApplicationStatus (@RequestBody HardwareMetrics hardwareMetrics) {
	 return  hardwareMetricsRepository.save(hardwareMetrics);
	}
}
